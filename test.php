<?php

//LIMESHARP TEST

function repeat ($arrayx)
{
    $array = $arrayx;
    $tripleArray = [];
    
    for ($i=0; $i < 3; $i++) { 
        foreach ($array as $value) {
            array_push($tripleArray,$value);
        }
    }

    echo "Triple Array: <br>";
    print_r($tripleArray);
    echo "<hr>";

    return $tripleArray;
}


function reformat ($stringx)
{
    $string = $stringx;
    
    echo "Original String -> ".$string."<br>";
    //list of forbidden vowels
    $vowels = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U");
    //replace vowels with empty space
    $string = str_replace($vowels, "", $string);
    //just lowercase the whole string
    $string = strtolower( $string );
    //capitalize!
    $string = ucfirst($string);

    echo "Reformatted string -> ".$string."<hr>";
    return $string;
}



function next_binary_number($arrayx)
{
    $binaries = $arrayx;
    $count = 0;
    $lastOne = 0;
    $arrayCount = count($binaries);
    $arrayCount = $arrayCount-1;
    $arrayPos = [];
    $arrayPos2 = [];
    $varStop = false;
    $j = 0;
    
    //check if array is full of 1's
    if(array_sum($binaries) == count($binaries)) {
        for ($i=0; $i <= $arrayCount ; $i++) 
             $binaries[$i] = 0;
            array_unshift($binaries,1);
    } 
    else 
    {   //loop trough the array :)
        for ($i=$arrayCount; $i >= 0  ; $i--) { 
            //two or more consecutives 1 and save positions
            if ($binaries[$i] == 1 && $binaries[$i-1] == 1 && $varStop == false) {
                    $arrayPos[$j] = $i;
                    $arrayPos2 = $arrayPos;
                    $j++;
            }
            //a 1 with a 0 on left
            if ($binaries[$i] == 1 && $binaries[$i-1] == 0) {
                    $varStop = true;
                    $arrayPos[$j] = $i;
                    $arrayPos2 = $arrayPos;
                    $binaries[$i-1] = 1;
                    break;
            }
            // just a 0
            if ($binaries[$i] == 0) {
                $binaries[$i] = 1;
                break;
            }
            //just die if not valid binary
            if ($binaries[$i] != 1 && $binaries[$i] != 0 ) {
                echo "Not valid binary number ";
                die;
            }

        }
        //we turn 0 all those consecutive 1 (no break)
        foreach ($arrayPos2 as $key => $value) {
            $binaries[$value] = 0;
        }
        
    }

    printArray($binaries);
    return $binaries;
}

//just a function to print values of an simple array
function printArray($array)
{
    foreach ($array as $key => $value) {
        echo $value;
    }
    echo "<br>";
}





//variables for testing
/*
$array1 = [1,"the string", 0.2, array(5,6),[1,2,"string"]];
$string1 = "liMeSHArp DeveLoper TEST";
$array2 = [1,0,0,0,1,1,0,0,0,1,1,0,1,1,1,1,1];



repeat($array1);
reformat($string1);
next_binary_number($array2);
*/




?>
<!DOCTYPE html>
<html>
    <head>
    <script src="test.js"></script>
    </head>
    <body>
        
    </body>
</html>